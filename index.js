'use strict'

const connect = require('connect')
const cors = require('cors')
const istanbulMiddleware = require('istanbul-middleware')

const app = connect()
app.use(cors())
app.use(istanbulMiddleware.createHandler())
app.listen(process.env.PORT || 3000, function () {
  console.log('Listening on ', this.address())
})
